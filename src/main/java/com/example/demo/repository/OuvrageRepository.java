package com.example.demo.repository;

import com.example.demo.entities.Ouvrage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OuvrageRepository extends JpaRepository<Ouvrage,Long> {
}
