/**
 * 
 */
package com.project.pidev.repository;

import java.util.Optional;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.project.pidev.entities.User;

/**
 * @author Marouen Ghozzi
 *
 */

@Repository
@Component
public interface UserRepository extends JpaRepository<User, Integer> {
	
	

	Optional<User> findByUserName(String username) ;
	Optional<User> findByEmail(String email);
    


	
		
}
