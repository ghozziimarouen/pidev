/**
 * 
 */
package com.project.pidev.repository;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.project.pidev.entities.TokenSecure;

/**
 * @author Marouen Ghozzi
 *
 */

@Repository
public interface TokenSecureRepositorty extends JpaRepository<TokenSecure, Long>{
 
}
