/**
 * 
 */
package com.example.demo.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;




@Entity
@Table(name = "User", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column
    @NotNull(message = "Name cannot be null")
	private String name ;
	
	@Column
    @NotNull(message = "surname cannot be null")
	private String surname ;
	
	@Column
	private String userName;
	
	
	@Column
	@NotNull
	@NotBlank(message = "password is a mandatory field")
	private String password;
	
	@Column
	@NotNull
	@NotBlank(message = "confirm password is mandatory ")
	@Length(min = 8 , message = "password must be at least 8 carractere" )
	private String confirmpassword ;
	
	@Column
	@NotNull
    @Email(message = "Email should be valid")
	@NotBlank(message = "Email is a mandatroy field")
	private String email;
	

	@Column
	private boolean active;
	
	/**
	 * @return the roles
	 */
	public List<Role> getRoles() {
		return roles;
	}
	
	//@OneToMany(mappedBy = "user")
	//private List<TokenSecure> tokenSecures ;

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	
	}
	

	
	

	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinTable(name = "user_roles" , joinColumns = @JoinColumn(name = "user_id"),
	inverseJoinColumns = @JoinColumn( name = "role_id"))
	//@Column(name = "role")
	 private List<Role> roles = new ArrayList<>() ;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the active
	 */
	public boolean isActive() {
		
		return true;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
/**
	 * @return the userName
	 */
	public String getUserName() {
		return setUserName(this.name,this.surname );
	}

	/**
	 * @param userName the userName to set
	 */
	public String setUserName(String name , String surname) {
		return name.concat(surname) ;
	}
/**
	 * @return the confirmpassword
	 */
	public String getConfirmpassword() {
		return confirmpassword;
	}

	/**
	 * @param confirmpassword the confirmpassword to set
	 */
	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}
/**
	 * @param id
	 * @param userName
	 * @param password
	 * @param email
	 * @param roles
	 */
	public User( @NotNull String name,@NotNull String surname,@NotNull String userName, @NotNull String password,@NotNull String confirmpassword, @NotNull String email,
			List<Role>roles) {
		
		super();
		this.name = name;
		this.surname=surname;
		this.userName=userName ;
		this.password = password;
		this.confirmpassword=confirmpassword;
		this.email = email;
		this.roles=roles ;
		
	}
	/**
	 * default constructor
	 */
	public User() {
		super();
	}
	
}

