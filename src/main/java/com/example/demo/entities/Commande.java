/**
 * 
 */
package com.example.demo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @author Marouen Ghozzi
 *
 */

@Entity
public class Commande {
	  @Id 
	  @GeneratedValue(strategy = GenerationType.AUTO)
	    private Long id;
	  
	  @ManyToMany(mappedBy = "commandes")
	  List<Ouvrage>ouvrages ;
	  
	  @OneToOne(cascade = CascadeType.ALL , fetch = FetchType.EAGER , mappedBy = "commande")
	  HistoriqueCommande historiqueCommande ;
	
	  @ManyToOne
	  Panier panier ;
}
