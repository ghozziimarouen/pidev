package com.example.demo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
@Entity
public class Ouvrage implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private double prix;
    private String auteur;
    @Temporal(TemporalType.DATE)
    private Date datePublication;
    private String categorie;
    private String titre;



    @OneToOne(mappedBy = "ouvrage")
    Offre offre ;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "commandes_ouvrages",
            joinColumns = @JoinColumn(name = "commande_id" , referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ouvrage_id" , referencedColumnName = "id" )
    )
    List<Commande>commandes ;


    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Offre getOffre() {
        return offre;
    }

    public void setOffre(Offre offre) {
        this.offre = offre;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ouvrage(double prix, String auteur, Date datePublication, String categorie) {
        this.prix = prix;
        this.auteur = auteur;
        this.datePublication = datePublication;
        this.categorie = categorie;
    }

    public Ouvrage() {
    }

    @Override
    public String toString() {
        return "Ouvrage{" +
                ", prix=" + prix +
                ", auteur='" + auteur + '\'' +
                ", datePublication=" + datePublication +
                ", categorie='" + categorie + '\'' +
                '}';
    }
}

