/**
 * 
 */
package com.example.demo.entities;

/**
 * @author Marouen Ghozzi
 *
 */
public enum TypeReclamation {
	
	WrongInformation,
	FRAUD,
	OTHER

}
