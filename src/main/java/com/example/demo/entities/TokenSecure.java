/**
 * 
 */
package com.project.pidev.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

/**
 * @author Marouen Ghozzi
 *
 */
@Component
@Entity 
@Table(name = "token")
public class TokenSecure  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id ;
	
	@Column
	private String additionalInformationToken ;
	
	@ManyToOne
	@JoinColumn(name = "user_id" , referencedColumnName = "id")
	private User user ;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the additionalInformationToken
	 */
	public String getAdditionalInformationToken() {
		return additionalInformationToken;
	}

	/**
	 * @param additionalInformationToken the additionalInformationToken to set
	 */
	public void setAdditionalInformationToken(String additionalInformationToken) {
		this.additionalInformationToken = additionalInformationToken;
	}

	/**
	 * 
	 */
	public TokenSecure() {
		super();
 //    user.setId(id);
}
	
	
}