/**
 * 
 */
package com.example.demo.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author Marouen Ghozzi
 *
 */
@Entity
public class Reclamation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(cascade = CascadeType.MERGE , fetch = FetchType.EAGER) 
	
	@JsonBackReference
	Publication publication;
	
	
	@Column
	@NotNull(message = "title cannot be null !")

	private String title;

	@Column
	@NotNull(message = "description cannot be null !")
	private String description;

	@Column
	@NotNull(message = "type connot be null !")
	@Enumerated(EnumType.STRING)
	private TypeReclamation type;

	@Column
	@Enumerated(EnumType.STRING)
	@NotNull(message = "priority cannot be null !")

	private PriorityEnumReclamtion priorityEnum;

	@Column
	private String status;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the publication
	 */
	public Publication getPublication() {
		return publication;
	}

	/**
	 * @param publication the publication to set
	 */
	public void setPublication(Publication publication) {
		this.publication = publication;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public TypeReclamation getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(TypeReclamation type) {
		this.type = type;
	}

	/**
	 * @return the priorityEnum
	 */
	public PriorityEnumReclamtion getPriorityEnum() {
		return priorityEnum;
	}

	/**
	 * @param priorityEnum the priorityEnum to set
	 */
	public void setPriorityEnum(PriorityEnumReclamtion priorityEnum) {
		this.priorityEnum = priorityEnum;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param publication
	 * @param title
	 * @param description
	 * @param type
	 * @param priorityEnum
	 * @param status
	 */
	public Reclamation(Publication publication, @NotNull(message = "title cannot be null !") String title,
			@NotNull(message = "description cannot be null !") String description,
			@NotNull(message = "type connot be null !") TypeReclamation type,
			@NotNull(message = "priority cannot be null !") PriorityEnumReclamtion priorityEnum, String status) {
		super();
		this.publication = publication;
		this.title = title;
		this.description = description;
		this.type = type;
		this.priorityEnum = priorityEnum;
		this.status = status;
	}

	/**
	 * default constructor
	 */
	public Reclamation() {
		super();
	}
	
	

	
	
}


