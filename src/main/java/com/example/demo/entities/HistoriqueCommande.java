/**
 * 
 */
package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * @author Marouen Ghozzi
 *
 */
@Entity
public class HistoriqueCommande {

	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	
	@OneToOne
	Commande commande ;
}
