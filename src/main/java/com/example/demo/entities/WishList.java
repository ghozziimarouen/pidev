/**
 * 
 */
package com.example.demo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author Marouen Ghozzi
 *
 */
@Entity
public class WishList {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	
	
	@OneToMany(cascade = CascadeType.ALL
			,fetch = FetchType.EAGER)
	List<Ouvrage> ouvrages ;
}

