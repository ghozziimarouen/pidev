/**
 * 
 */
package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.bytebuddy.dynamic.loading.ClassReloadingStrategy.Strategy;

/**
 * @author Marouen Ghozzi
 *
 */
@Entity
@Table
public class FeedBack {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;

	@ManyToOne
	Publication publication ;
}

