package com.example.demo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "publication")
public class Publication {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String nom;

	@Column
	private String description;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Ouvrage ouvrage;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "publication")
	List<FeedBack> feedBacks;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "publication", fetch = FetchType.LAZY)
	@JsonManagedReference



	List<Reclamation> reclamations;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the ouvrage
	 */
	public Ouvrage getOuvrage() {
		return ouvrage;
	}

	/**
	 * @param ouvrage the ouvrage to set
	 */
	public void setOuvrage(Ouvrage ouvrage) {
		this.ouvrage = ouvrage;
	}

	/**
	 * @return the feedBacks
	 */
	public List<FeedBack> getFeedBacks() {
		return feedBacks;
	}

	/**
	 * @param feedBacks the feedBacks to set
	 */
	public void setFeedBacks(List<FeedBack> feedBacks) {
		this.feedBacks = feedBacks;
	}

	/**
	 * @return the reclamations
	 */
	public List<Reclamation> getReclamations() {
		return reclamations;
	}

	/**
	 * @param reclamations the reclamations to set
	 */
	public void setReclamations(List<Reclamation> reclamations) {
		this.reclamations = reclamations;
	}

}
