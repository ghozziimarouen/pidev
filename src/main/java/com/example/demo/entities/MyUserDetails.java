package com.example.demo.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class MyUserDetails implements UserDetails {
	
	
	/**
		 * 
		 */
		
		
		
		private String name ;
		

		public MyUserDetails(String name, String surname, String userName, String password, String confirmpassword,
			boolean active) {
		super();
		this.name = name;
		this.surname = surname;
		this.userName = userName;
		this.password = password;
		this.confirmpassword = confirmpassword;
		this.active = active;
		
	}
		/**
		 * @param user
		 */ 
		public MyUserDetails(User user) {
			super();
			this.user = user;
		}
		

		private String surname ;
		
		
		private String userName;

		
		private String password;

		
		private String confirmpassword ;

		
		private boolean active;
		
		private User user ;


	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<Role>roles = user.getRoles() ;
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		for (Role role :roles) {
			authorities.add(new SimpleGrantedAuthority(role.getName())) ;
			
		}
		return authorities ;
	}

}
