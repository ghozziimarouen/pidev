/**
 * 
 */
package com.example.demo.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Marouen Ghozzi
 *
 */

@Entity
@Table
public class Panier {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id  ;
	
	
	@OneToMany(cascade = CascadeType.ALL , fetch = FetchType.EAGER , mappedBy = "panier" )
	List<Commande> commandes ;
	
}
