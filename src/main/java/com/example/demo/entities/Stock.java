package com.example.demo.entities;

import javax.persistence.*;

import java.util.Collection;
@Entity
@Table(name = "stock")
public class Stock {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int  quantité;
    //un service qui envoie un mail quand la quantité est inferieur a 50
    private final int quantity_de_securité =50;





    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idOuvrage", referencedColumnName = "id")
    private Ouvrage ouvrage;

}
