
package com.project.pidev.security.configuration;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.project.pidev.repository.UserRepository;



@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@Configuration
@EnableWebSecurity
@Component
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final UserRepository userRepo;

	@Autowired
	UserDetailsService userDetailsService;

	public SecurityConfiguration(UserRepository userRepo) {
		this.userRepo = userRepo;
	}

	// AUTHENTIFCATION using the loadbyusername in userdeatilsService
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
		
	}

	//authorisation
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
	
//		authorizeRequests().antMatchers("/user/registeradmin").hasAuthority("ADMIN")
//				.antMatchers("/user/registersimpleuser").permitAll().antMatchers("	/authenticate").permitAll()

//		authorizeRequests()	.antMatchers("/authenticate").permitAll()
//		.antMatchers("/user/listallusers").hasAuthority("ADMIN").and().httpBasic().and().csrf().disable();
	

//http.httpBasic().and().authorizeRequests().antMatchers(HttpMethod.POST, "registersimpleuser").permitAll();
//http.httpBasic().and().authorizeRequests().antMatchers(HttpMethod.POST, "authenticate").permitAll();
//http.httpBasic().and().authorizeRequests().antMatchers(HttpMethod.GET, "listallusers").hasRole("ADMIN");
//http.csrf().disable();
						
		
//	}
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/**");
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

//	@Override
//	public void configure(WebSecurity web) throws Exception {
//		web.ignoring().antMatchers("/**");
//	}

	public BCryptPasswordEncoder gePasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
	
	

}
