/**
 * 
 */
package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entities.Reclamation;
import com.example.demo.repository.ReclamationRepository;

/**
 * @author Marouen Ghozzi
 *
 */
@Service
public class ReclamationService {
	public static final String STATUSOpen = "OPEN";
	public static final String STATUSINPROGRESS = "INPROGRESS";
	public static final String StatusClosed = "CLOSED";
	@Autowired
	ReclamationRepository reclamationRepository;

	public Reclamation saveReclamation(Reclamation reclamation) {
		Reclamation reclamationAlerted = new Reclamation(reclamation.getPublication(), reclamation.getTitle(),
				reclamation.getDescription(), reclamation.getType(), reclamation.getPriorityEnum(),
				reclamation.getStatus());
		reclamationAlerted.setStatus(STATUSOpen);
		reclamationRepository.save(reclamationAlerted);
		return reclamationAlerted;
	}

	public List<Reclamation> findAllReclamation() {
		return reclamationRepository.findAll();
	}

	public Reclamation updateReclamation(long id, Reclamation Newreclamation) {
		if (reclamationRepository.findById(id).isPresent()) {
			Reclamation existingReclamation = reclamationRepository.findById(id).get();
			existingReclamation.setTitle(Newreclamation.getTitle());
			existingReclamation.setDescription(Newreclamation.getDescription());
			existingReclamation.setPriorityEnum(Newreclamation.getPriorityEnum());
			existingReclamation.setType(Newreclamation.getType());

			
			return reclamationRepository.save(existingReclamation);
		} else
			return null;
	}
	public String deleteReclamation (Long id) {
		if (reclamationRepository.findById(id).isPresent()) {
			reclamationRepository.deleteById(id);
			return "Reclamation supprimé";
			
		}
		else 
		return "Reclamation non supprimé";
		}

}
