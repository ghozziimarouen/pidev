/**
 * 
 */
package com.project.pidev.services;


import java.util.Arrays;
import java.util.List;

import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.pidev.entities.Role;
import com.project.pidev.entities.User;
import com.project.pidev.repository.UserRepository;

/**
 * @author Marouen Ghozzi
 *
 */
@Component
@Service   
public class UserService  {
	
	public static final String roleAdmin = "ADMIN" ;
	public static final String roleUser = "USER" ;

		
	@Autowired
	UserRepository userRepository ;
	
	public User saveUser (User user ) {
		User userRegistred = new User( user.getName() ,user.getSurname(),user.getUserName(), user.getPassword(),user.getConfirmpassword(), user.getEmail() , 
				Arrays.asList(new Role(roleUser))  ) ;
		return userRepository.save(userRegistred) ;
	}
	
	public User saveAdmin(User user) {
		User adminRegistred = new User( user.getName() ,user.getSurname(),user.getUserName(), user.getPassword(),user.getConfirmpassword(), user.getEmail() , 
				Arrays.asList(new Role(roleAdmin)) ) ;
		return userRepository.save(adminRegistred) ;
	}
	
	public List<User> findAllUsers () {
		
		return userRepository.findAll() ;
	}
	
		
	

}
