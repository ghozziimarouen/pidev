package com.example.demo.services;

import com.example.demo.entities.Ouvrage;
import com.example.demo.repository.OuvrageRepository;
import com.example.demo.repository.PublicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OuvrageService {

	@Autowired
	private OuvrageRepository ouvrageRepository ;

	public Ouvrage addOuvrage(Ouvrage ouvrage) {
		return ouvrageRepository.save(ouvrage);
	}
	public List<Ouvrage> getOuvrage () {
		return ouvrageRepository.findAll();
	}
	
	
}
