/**
 * 
 */
package com.project.pidev.security.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.project.pidev.entities.MyUserDetails;
import com.project.pidev.entities.User;
import com.project.pidev.repository.UserRepository;

/**
 * @author Marouen Ghozzi
 *
 */


@Component
@Service
public class UserDetailService implements UserDetailsService {
	
	@Autowired
	UserRepository userRepository ;
	
	
	  
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<User> user =	userRepository.findByUserName(username) ;
		
		user.orElseThrow(()-> new UsernameNotFoundException("user with user name" + username +"not found")) ;
		
		
		
		return user.map(MyUserDetails::new).get() ;	
	}
	
	
	public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
		
		Optional<User> user =	userRepository.findByEmail(email) ;
		
		user.orElseThrow(()-> new UsernameNotFoundException("user with user name" + email +"not found")) ;
		
		
		
		return user.map(MyUserDetails::new).get() ;	
	}
	
	

}
