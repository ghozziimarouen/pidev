/**
 * 
 */
package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Reclamation;
import com.example.demo.services.ReclamationService;

/**
 * @author Marouen Ghozzi
 *
 */
@RestController
@RequestMapping(value = "/reclamation", produces = "application/json", method = { RequestMethod.GET, RequestMethod.POST,
		RequestMethod.PUT })
public class ReclamationController {

	@Autowired
	ReclamationService reclamationService;

	@PostMapping(value = "/addreclamation")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Reclamation> createReclamation(@RequestBody Reclamation reclamation) {

		return new ResponseEntity<>(reclamationService.saveReclamation(reclamation), HttpStatus.OK);
	}

	@RequestMapping(value = "/listallreclamations", produces = "application/json", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public List<Reclamation> findAllReclamation() {
		return reclamationService.findAllReclamation();
	}

	@PutMapping(value = "updatereclamation/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Reclamation> updateReclamation(@PathVariable(value = "id") Long id,
			@RequestBody Reclamation reclamation) {
		return new ResponseEntity<>(reclamationService.updateReclamation(id, reclamation), HttpStatus.OK);
	}

	@DeleteMapping(value = "deletereclamation/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> deleteReclamation(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<>(reclamationService.deleteReclamation(id), HttpStatus.OK);
	}
}
