/**
 * 
 */
package com.project.pidev.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.project.pidev.customExceptions.InvalidPasswordConfirmationException;
import com.project.pidev.entities.ConfirmationToken;
import com.project.pidev.entities.User;
import com.project.pidev.repository.ConfirmationTokenRepository;
import com.project.pidev.repository.UserRepository;
import com.project.pidev.security.services.JwtRequest;
import com.project.pidev.security.services.JwtTokenUtil;
import com.project.pidev.security.services.UserDetailService;
import com.project.pidev.services.EmailSenderService;
import com.project.pidev.services.UserService;

/**
 * @author Marouen Ghozzi
 *
 */
@RestController
@RequestMapping(value = "/user", produces = "application/json", method = { RequestMethod.GET, RequestMethod.POST })

public class UserControllerImpl {
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailService userDetailsService;
	
	@Autowired
	private EmailSenderService emailSenderService;
	
	
	
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);



	@PostMapping(value = "/registersimpleuser")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<User> createUser(@RequestBody User user) throws InvalidPasswordConfirmationException {

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		if (user.getPassword() != null && user.getConfirmpassword() != null) {
			if (!user.getPassword().equals(user.getConfirmpassword())) {
				throw new InvalidPasswordConfirmationException("password doesn't match !");

			}
		}
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		String encodedConfirmedPassword = passwordEncoder.encode(user.getConfirmpassword());
		user.setConfirmpassword(encodedConfirmedPassword);
		
		return new ResponseEntity<>(userService.saveUser(user), HttpStatus.OK);
	}
	
	@PostMapping(value = "/registeradmin")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<User> createAdmin(@RequestBody User user) throws Exception {

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	//	String encodedPassword = passwordEncoder.encode(user.getPassword());
		if (user.getPassword() != null && user.getConfirmpassword() != null) {
			if (!user.getPassword().equals(user.getConfirmpassword())) {
				throw new InvalidPasswordConfirmationException("password doesn't match !");
			}
		}
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		String encodedConfirmedPassword = passwordEncoder.encode(user.getConfirmpassword());
		user.setConfirmpassword(encodedConfirmedPassword);
		user.setPassword(encodedPassword);
		return new ResponseEntity<>(userService.saveAdmin(user), HttpStatus.OK);
	}
	@RequestMapping(value = "/listallusers", produces = "application/json", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public List<User> findAllUsers() {
		return userService.findAllUsers();
		
	}
	
	@RequestMapping(value="/forgot-password", method=RequestMethod.POST)
	public String createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());
		final String userDetailsmail = userRepository.findByUserName(authenticationRequest.getUsername()).get().getEmail();
		
		 final Authentication authentication = authenticationManager.authenticate(
	                new UsernamePasswordAuthenticationToken(
	                		authenticationRequest.getUsername(),
	                		authenticationRequest.getPassword()
	                )
	        );
	        SecurityContextHolder.getContext().setAuthentication(authentication);
	       		
			// create token
			
			
			// save it
			
	        final String token = jwtTokenUtil.generateToken(userDetails);
			// create the email
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(userDetailsmail);
			mailMessage.setSubject("Complete Password Reset!");
			mailMessage.setFrom("tarek.chehidi@esprit.com");
			mailMessage.setText("you can login with a new token , please click here: "
			+"http://localhost:7080/authenticate/"+token);
			
			emailSenderService.sendEmail(mailMessage);
			
			return "mail sent";


	}
		
	
	private void authenticate(String email, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	}
	

	

