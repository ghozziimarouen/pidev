package com.example.demo.controller;

import com.example.demo.entities.Ouvrage;
import com.example.demo.services.OuvrageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OuvrageController {
	
	@Autowired
	private OuvrageService ouvrageService ;

	@PostMapping("/addouvrage")
	public Ouvrage addOuvrage (@RequestBody Ouvrage ouvrage) {
		return ouvrageService.addOuvrage(ouvrage);
	}
	@GetMapping("/showouvrage")
	public List<Ouvrage> getOuvrage(){
		return ouvrageService.getOuvrage();
	}

}
